using System;
using System.Collections;
using System.Linq;

namespace console01
{
    class WordProcessor : IWordProcessor
    {
        public string Capitalize(string str, bool shouldTrim, out string errorMsg)
        {
        	if (str == null)
        	{
        		errorMsg = "Error: string is null";
        		return String.Empty;
        	}
        	errorMsg = String.Empty;
        	str = str.ToUpper();
        	if (shouldTrim)
        		str = str.Trim();
        	return str;
        }

        public string Inverse(string str, bool removeSpaces, out string errorMsg)
        {
        	if (str == null)
        	{
        		errorMsg = "Error: string is null";
        		return String.Empty;
        	}
        	errorMsg = String.Empty;
        	if (removeSpaces)
        		str = str.Replace(" ", "");
        	str = new string(Enumerable.Range(1, str.Length).Select(i => str[str.Length - i]).ToArray());
        	return str;
        }

        public string[] ParseWords(string str, out string errorMsg)
        {
        	errorMsg = String.Empty;
        	if (string.IsNullOrEmpty(str)) return new string[0];
        	return str.ToLower().Split(new char[] { ' ', '\t', '.', ',', ';', '!', '?' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
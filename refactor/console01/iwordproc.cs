using System;

namespace console01
{
    interface IWordProcessor
    {
        string Capitalize(string str, bool shouldTrim, out string errorMsg);

        string Inverse(string str, bool removeSpaces, out string errorMsg);

        string[] ParseWords(string str, out string errorMsg);
    }
}
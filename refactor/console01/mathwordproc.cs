using System;
using System.Collections;
using System.Linq;

namespace console01
{
    class MathWordProcessor : IWordProcessor
    {
        public string Capitalize(string str, bool shouldTrim, out string errorMsg)
        {
        	throw new NotImplementedException("");
        }

        public string Inverse(string str, bool removeSpaces, out string errorMsg)
        {
        	if (string.IsNullOrEmpty(str))
                throw new Exception("'str' should not be null or empty");
            errorMsg = String.Empty;
            if (removeSpaces)
                str = str.Replace(" ", "");
            str = new string(Enumerable.Range(1, str.Length).Select(i => str[str.Length - i]).ToArray());
            return str;
        }

        public string[] ParseWords(string str, out string errorMsg)
        {
        	errorMsg = String.Empty;
        	if (string.IsNullOrEmpty(str)) return new string[0];
        	return str.ToLower().Split(new char[] { ' ', '\t', '.', ',', ';', '!', '?' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public int ComparePhrases(string a, string b)
        {
            string[] clearPhrases = { a, b };
            string errorMsg;
            for (var i = 0; i < 2; i++)
                clearPhrases[i] = string.Join(" ", ParseWords(clearPhrases[i], out errorMsg)).ToLower();
            return string.Compare(clearPhrases[0], clearPhrases[1]);
        }

        public double CalculateRW(double step, int numSteps, double kCrx = 0.5)
        {
            return Math.Pow(numSteps, kCrx) * step;
        }
    }
}
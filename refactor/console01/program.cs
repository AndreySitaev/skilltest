using System;

namespace console01
{
    class Program
    {
        static void Main(string[] args)
        {
        	var proc = GetProcessor();
        	string src = " Was it a car or a cat I saw?";
        	string errorMsg;
        	Console.WriteLine("1:" + proc.Capitalize(src, true, out errorMsg));
        	Console.WriteLine("2:" + proc.Inverse(src, false, out errorMsg));
        	Console.WriteLine("3:" + string.Join(", ", proc.ParseWords(src, out errorMsg)));
        	if (proc is MathWordProcessor)
        		Console.WriteLine("4:" + ((MathWordProcessor) proc).CalculateRW(1, 9).ToString("F3"));
            Console.ReadLine();
        }

        static IWordProcessor GetProcessor()
        {
        	return new WordProcessor();
        }
    }
}